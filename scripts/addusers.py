from authentication.models import UserProfile
import os, csv 
import pandas as pd
from django.contrib.auth.models import User
from django.core.files import File

def run():
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    data = pd.read_csv(BASE_DIR+'/core/data_csv/users.csv')
    dict_of_lists = {}
    for column_name in data.columns:
        temp_list = data[column_name].tolist()
        dict_of_lists[column_name] = temp_list
    with open(BASE_DIR+'/core/data_csv/users.csv') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            full_name = row['fullName'].split(' ')
            try:
                last_name = full_name[1]
            except:
                last_name= "None"    
            user = User(
                username = row['userEmail'],
                first_name = full_name[0],
                last_name = last_name,
                email = row['userEmail']
            )
            user.save()
            user.set_password = row['password']
            user.save()
            userprofile = UserProfile(    user = user,
                contactNo = row['contactNo'],
                address = row['address'],
                Location = row['Location'],
                country = row['country'],
                empcode = row['empcode'],
                # userImage = row[''],
                regDate = row['regDate'],
                updationDate = row['updationDate'],
            )
            userprofile.save()