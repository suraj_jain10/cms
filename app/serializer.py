from rest_framework import serializers
from .models import complaints, complaintRemark, materialRequest, materialRemark


class complaintRemarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = complaintRemark

        fields=('id',
            'status',
            'remark',
            'remarkDate',
            'updationDate',

        )



class complaintsSerializer(serializers.ModelSerializer):
    remarks = complaintRemarksSerializer(many=True, read_only=True)
    

    class Meta:
        model = complaints

        fields=('id',
            'complaintNumber',
            'userId',
            'complaintType',
            'state',
            'description',
            'complaintDetails',
            'complaintFile',
            'status',
            'regDate',
            'lastUpdationDate',
            'remarks',

        )

class materialRemarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = materialRemark
        fields = (
            'id',
            'status',
            'remark',
            'remarkDate',
            'lastupdationDate',
        )        

class materialRequestSerializer(serializers.ModelSerializer):
    materialRemark = materialRemarkSerializer(many=True, read_only=True)
    class Meta:
        model = materialRequest
        fields = ( 'id',
            'userId',
            'material',
            'reason',
            'when_req',
            'status',
            'requestedDate',
            'updationDate',
            'materialRemark',
        )