from django.db import models
from django.contrib.auth.models import User

# Create your models here.



class category(models.Model):
    categoryName= models.CharField(max_length=200, null=True, blank=True)
    categoryDescription= models.CharField(max_length=200, null=True, blank=True)
    creationDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    updationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.categoryName 

class subcategory(models.Model):
    categoryName= models.ForeignKey(category, on_delete=False, default="", null=True, blank=True)
    subcategory= models.CharField(max_length=200, null=True, blank=True)
    creationDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    updationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    def __str__(self):
        return self.subcategory        
        

class complaints(models.Model):
    complaintNumber = models.CharField(max_length=500, null=True, blank=True)
    userId =  models.ForeignKey(User, related_name="complaint_requested_by", on_delete=False, default="", null=True, blank=True)
    complaintType = models.CharField(max_length=500, null=True, blank=True)
    state = models.CharField(max_length=500, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    complaintDetails = models.CharField(max_length=1500, null=True, blank=True)
    complaintFile = models.FileField(upload_to='complaints/', null=True, blank=True)
    STATUS_CHOICES =( 
        ("InProcess", "In-Process"), 
        ("pending", "pending"), 
        ("resolved", "resolved"), 
    ) 
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default = 'pending')
    regDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    lastUpdationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.complaintNumber        
               
class complaintRemark(models.Model):
    complaintNumber = models.ForeignKey(complaints, related_name='remarks',on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=500, null=True, blank=True)
    remark = models.CharField(max_length=500, null=True, blank=True)
    remarkDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    updationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.complaintNumber.complaintNumber


class materialRequest(models.Model):
    userId = models.ForeignKey(User, related_name='material_request_user',on_delete=models.CASCADE, null=True, blank=True)
    material = models.CharField(max_length=500, null=True, blank=True)
    reason = models.CharField(max_length=500, null=True, blank=True)
    when_req = models.CharField(max_length=500, null=True, blank=True)
    STATUS_CHOICES =( 
        ("requested", "requested"), 
        ("inprocess", "In-Process"), 
        ("material", "material recieved"), 
    ) 
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default = 'requested')
    requestedDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    updationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.material

class materialRemark(models.Model):
    material = models.ForeignKey(materialRequest, related_name='materialRemark',on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=500, null=True, blank=True)
    remark = models.CharField(max_length=500, null=True, blank=True)
    remarkDate = models.DateTimeField(auto_now=False, null=True, blank=True)
    lastupdationDate = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.material.material        