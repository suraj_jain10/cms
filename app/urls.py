from django.urls import path, re_path
from app import views

urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('addCategory/', views.addcategory, name='addcategory'),
    path('logcomplaint/', views.logComplaint, name='logComplaint'),
    path('manageuser/', views.manageUsers, name='manageUsers'),

    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),

]
